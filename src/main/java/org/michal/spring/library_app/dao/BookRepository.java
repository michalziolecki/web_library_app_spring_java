package org.michal.spring.library_app.dao;

import org.michal.spring.library_app.models.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BookRepository extends CrudRepository<Book, Integer> {

    @Query(value = "insert into books (id, book_name, author, purchase) " +
            "values (1,'Podstawy Java', 'autor', '2018-09-05');", nativeQuery = true)
    void insertTestingBean();

    @Query(value = "delete from books where id > 1 ;", nativeQuery = true)
    void deleteAllToTest();


}
